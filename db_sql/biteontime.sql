DROP DATABASE IF EXISTS biteontime;

CREATE DATABASE biteontime;

USE biteontime;

CREATE TABLE PRODUCT (
	productCode VARCHAR(10),
	productName VARCHAR(50),
	catalogCategory VARCHAR(50),
	price decimal,
	description VARCHAR(200),
	imageUrl VARCHAR(100),
	PRIMARY KEY (productCode)
);

CREATE TABLE USER (
	userID INT NOT NULL AUTO_INCREMENT,
	firstName VARCHAR(50),
	lastName VARCHAR(50),
	emailAddress VARCHAR(100),
	addressField1 VARCHAR(80),
	addressField2 VARCHAR(80),
	city VARCHAR(50),
	state VARCHAR(50),
	zipPostalCode VARCHAR(50),
	country VARCHAR(50),
	password VARCHAR(50),
	PRIMARY KEY (userID)
);

CREATE USER biteontime_user@localhost IDENTIFIED BY 'biteontime';

GRANT ALL
ON biteontime.*
TO biteontime_user@localhost;


GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP
ON biteontime.*
TO biteontime_user@localhost;

