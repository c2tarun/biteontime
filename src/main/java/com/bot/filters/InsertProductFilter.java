/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bot.filters;

import com.bot.db.CatalogDB;
import com.bot.models.Catalog;
import java.io.IOException;
import java.util.List;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author tarun
 */
public class InsertProductFilter implements Filter{
    
    private FilterConfig filterConfig = null;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.filterConfig = filterConfig;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        
        String servletPath = httpRequest.getServletPath();
        if(servletPath != null & servletPath.endsWith("insertProduct.jsp")) {
            CatalogDB cdb = new CatalogDB();
            List<Catalog> catalogs = cdb.getCatalogs();
            request.setAttribute("catalogs", catalogs);
        }
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
        
    }
    
}
