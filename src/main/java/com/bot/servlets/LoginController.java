/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bot.servlets;

import static com.bot.db.Constants.*;
import com.bot.db.UserDB;
import com.bot.models.User;
import com.bot.util.PasswordUtil;
import java.io.IOException;
import java.io.PrintWriter;
import javax.persistence.NoResultException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author tarun
 */
public class LoginController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action=request.getParameter("action");
        if("logout".equals(action)) {
            HttpSession session = request.getSession();
            session.removeAttribute(USER);
            session.removeAttribute(SHOPPING_CART);
            getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
            return;
        }
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        HttpSession session = request.getSession();
        String loginError ="";
        password = PasswordUtil.hashPassword(password);

        UserDB udb = new UserDB();
        User user = null;
        try {
            user = udb.getUserByEmail(email);
        } catch (NoResultException e) {
            loginError += "Username/Password Incorrect";
            getServletContext().getRequestDispatcher("/signup.jsp").forward(request, response);
        }
        if (user.getPassword().equals(password)) {
            session.setAttribute(USER, user);
            session.removeAttribute(SHOPPING_CART);
            if(user.getIsAdmin() == 1) {
                getServletContext().getRequestDispatcher("/admin.jsp").forward(request, response);
                return;
            }
            String fwdString = (String) session.getAttribute(FWD_STR);
            if(fwdString != null) {
                getServletContext().getRequestDispatcher(fwdString).forward(request, response);
                return;
            }
            response.sendRedirect("/biteontime/CatalogControllerServlet");
            return;
        } else {
            loginError += "Username/Password Incorrect";
            request.setAttribute("loginError", loginError);
            request.setAttribute("email", email);
            getServletContext().getRequestDispatcher("/signup.jsp").forward(request, response);
            return;
        }
    }

}
