/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bot.servlets;

import com.bot.db.Constants;
import com.bot.db.OrderDB;
import com.bot.db.UserDB;
import com.bot.models.Order;
import com.bot.models.User;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author tarun
 */
public class AdminController extends HttpServlet implements Constants {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        HttpSession session = request.getSession();
        
        User user = (User) session.getAttribute(USER);
        String loginError = "";
        if(user == null || user.getIsAdmin() == 0 ) {
            loginError += "Login with Admin account";
            request.setAttribute("loginError", loginError);
            getServletContext().getRequestDispatcher("/signup.jsp").forward(request, response);
            return;
        }
        
        if(VIEW_ORDERS.equals(action)) {
            OrderDB odb = new OrderDB();
            List<Order> orders = odb.getAllOrders();
            UserDB udb = new UserDB();
            for(Order order : orders) {
                user = udb.getUserByUserID(order.getUserId());
                order.setUser(user);
            }
            
            session.setAttribute(THE_ORDERS, orders);
            getServletContext().getRequestDispatcher("/orderList.jsp").forward(request, response);
            return;
        } else {
            getServletContext().getRequestDispatcher("/admin.jsp").forward(request, response);
            return;
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
