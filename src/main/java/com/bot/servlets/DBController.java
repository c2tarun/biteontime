/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bot.servlets;

import com.bot.db.CatalogDB;
import com.bot.db.Constants;
import static com.bot.db.Constants.USER;
import com.bot.db.ProductDB;
import com.bot.db.UserDB;
import com.bot.models.Catalog;
import com.bot.models.Product;
import com.bot.models.User;
import com.bot.util.PasswordUtil;
import com.bot.util.Util;
import com.bot.validators.Validator;
import com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException;
import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;
import java.util.HashMap;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author tarun
 */
public class DBController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String action = request.getParameter("action");
        if ("addUserToDB".equals(action)) {
            String firstName = request.getParameter("firstName");
            String lastName = request.getParameter("lastName");
            String emailAddress = request.getParameter("emailAddress");
            String addr1 = request.getParameter("addr1");
            String addr2 = request.getParameter("addr2");
            String city = request.getParameter("city");
            String state = request.getParameter("state");
            String zip = request.getParameter("zip");
            String country = request.getParameter("country");
            String password = request.getParameter("password");
            String confirmPassword = request.getParameter("confirmPassword");
            String signUpError = "";
            if(firstName == null || firstName.length() == 0) {
                signUpError += "First Name missing<br>";
            }
            request.setAttribute("firstName", firstName);
            
            if(lastName == null || lastName.length() == 0) {
                signUpError += "Last Name missing<br>";
            }
            request.setAttribute("lastName", lastName);
            
            if(emailAddress == null || emailAddress.length() == 0) {
                signUpError += "Email Address missing<br>";
            }
            request.setAttribute("emailAddress", emailAddress);
            
            if(addr1 == null || addr1.length() == 0) {
                signUpError += "Address Field missing<br>";
            }
            request.setAttribute("addr1", addr1);
            request.setAttribute("addr2", addr2);
            if(city == null || city.length() == 0) {
                signUpError += "City Field missing<br>";
            }
            request.setAttribute("city", city);
            if(state == null || state.length() == 0) {
                signUpError += "State Field missing<br>";
            }
            request.setAttribute("state", state);
            if(zip == null || zip.length() == 0) {
                signUpError += "Zip code missing<br>";
            }
            request.setAttribute("zip", zip);
            if(country == null || country.length() == 0) {
                signUpError += "Country Field missing<br>";
            }
            request.setAttribute("country", country);
            if(password == null || password.length() == 0) {
                signUpError += "Password Field missing<br>";
            }
            if(!password.equals(confirmPassword)) {
                signUpError += "Password Mismatch<br>";
            }
            request.setAttribute("signUpError", signUpError);
            if(!signUpError.equals("")) {
                
                getServletContext().getRequestDispatcher("/signup.jsp").forward(request, response);
                return;
            }
            MessageDigest md;
            try {
                md = MessageDigest.getInstance("SHA-256");
            } catch (Exception e) {
                
            }
            password = PasswordUtil.hashPassword(password);
            User user = new User(firstName, lastName, emailAddress,
                    addr1, addr2, city, state, zip, country, password);

            user.setIsAdmin(0);
            try{
            UserDB.insertUser(user);
            } catch (Exception e){
                signUpError += "User already exists";
                request.setAttribute("signUpError", signUpError);
                getServletContext().getRequestDispatcher("/signup.jsp").forward(request, response);
                return;
            }
            request.setAttribute("message", firstName + " added to DB");
            HttpSession session = request.getSession();
            session.setAttribute(USER, user);
            getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
            return;
        }
        if("addCatalogToDB".equals(action)){
            String catalogName = request.getParameter("catalogName");
            Catalog catalog = new Catalog(catalogName);
            CatalogDB.insertCatalog(catalog);
            request.setAttribute("message", catalogName + " added to DB");
            getServletContext().getRequestDispatcher("/insertCatalog.jsp").forward(request, response);
        }
        if("fetchCatalogs".equals(action)){
            CatalogDB cdb = new CatalogDB();
            List<Catalog> catalogs = cdb.getCatalogs();
            request.setAttribute("catalogs", catalogs);
            getServletContext().getRequestDispatcher("/insertCatalog.jsp").forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Code to add product to DB
        String errorMessages = "";
        String UPLOAD_DIRECTORY = Util.getImageUploadLocation();
        HashMap formValues = saveImage(request);
        String productCode = (String) formValues.get("productCode");
        request.setAttribute("productCode", productCode);
        String productName = (String) formValues.get("productName");
        request.setAttribute("productName", productName);
        String catalog = (String) formValues.get("catalog");
        request.setAttribute("catalog", catalog);
        String priceStr = (String) formValues.get("price");

        double price = 0;
        if (priceStr.length() > 0) {
            price = Double.parseDouble(priceStr);
        } else {
            errorMessages += "Price missing<br>";
        }
        String description = (String) formValues.get("description");
        request.setAttribute("description", description);
        File image = (File) formValues.get("image");
        String imageUrl = Constants.IMAGE_SERVLET + (String) formValues.get("imageName");

        Product product = new Product(productCode, productName, catalog, description, price, imageUrl);

        errorMessages += Validator.validateProduct(product);

        if (!Validator.validateImage(image)) {
            errorMessages += "Please upload image for product<br>";
        }
        request.setAttribute("price", price);
        if (errorMessages.length() <= 0) {
            try {
                ProductDB.insertProduct(product);
                image.renameTo(new File(UPLOAD_DIRECTORY + File.separator + (String) formValues.get("imageName")));
                request.setAttribute("message", productName + " added to DB");
            } catch (Exception e) {
                errorMessages += "Product Code exist";
                request.setAttribute("message", errorMessages);
                getServletContext().getRequestDispatcher("/insertProduct.jsp").forward(request, response);
            }

        } else {
            request.setAttribute("message", errorMessages);
        }

        getServletContext().getRequestDispatcher("/insertProduct.jsp").forward(request, response);
    }

    private HashMap saveImage(HttpServletRequest request) {
        HashMap formValues = new HashMap();
        String UPLOAD_DIRECTORY = Util.getImageUploadLocation();
        if (ServletFileUpload.isMultipartContent(request)) {
            String name = "";
            String newName = "";
            File temp = new File(UPLOAD_DIRECTORY + File.separator + "temp.jpg");
            try {
                FileItemFactory factory = new DiskFileItemFactory();
                ServletFileUpload upload = new ServletFileUpload(factory);
                List<FileItem> items = null;
                items = upload.parseRequest(request);

                for (FileItem item : items) {
                    if (!item.isFormField()) {
                        name = new File(item.getName()).getName();
                        item.write(temp);
                    } else {
                        formValues.put(item.getFieldName(), item.getString());
                        if ("productCode".equals(item.getFieldName())) {
                            newName = item.getString() + ".jpg";
                        }
                    }
                }
                //temp.renameTo(new File(UPLOAD_DIRECTORY + File.separator + newName));
            } catch (Exception e) {
            }
            formValues.put("imageName", newName);
            formValues.put("image", temp);
            return formValues;
        }
        return null;
    }

}
