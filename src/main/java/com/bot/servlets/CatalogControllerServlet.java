package com.bot.servlets;

import com.bot.db.CatalogDB;
import com.bot.db.ProductDB;
import com.bot.models.Catalog;
import com.bot.models.Product;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This servlets checks and return product java beans, 
 * based on the product code or product name in the
 * request object.
 */
public class CatalogControllerServlet extends HttpServlet {

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //doing all the work in doPost so transfer doGet call to doPost.
        doPost(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        CatalogDB cdb = new CatalogDB();
        List<Catalog> catalogs = cdb.getCatalogs();
        request.setAttribute("catalogs", catalogs);
        
        ProductDB productDb = new ProductDB();
        String productCode = (String) request.getParameter("productCode");
        if(productCode != null){
            Product product = productDb.getProductByProductCode(productCode);
            if(product != null){
                request.setAttribute("product", product);
                getServletContext().getRequestDispatcher("/viewProduct.jsp").forward(request,response);
                return;
            }
        }
        
        String category = (String) request.getParameter("catalogCategory");
        if(category != null){
            List<Product> products = productDb.getProductsByCategory(category);
            request.setAttribute("products", products);
            getServletContext().getRequestDispatcher("/catalogList.jsp").forward(request,response);
            return;
        }

        List<Product> products = productDb.getProducts();
        request.setAttribute("products", products);
        getServletContext().getRequestDispatcher("/catalogList.jsp").forward(request,response);  
        return;
    }

}
