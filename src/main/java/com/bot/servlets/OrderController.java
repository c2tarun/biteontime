/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bot.servlets;

import com.bot.db.Constants;
import com.bot.db.OrderDB;
import com.bot.db.UserDB;
import com.bot.models.Cart;
import com.bot.models.Order;
import com.bot.models.OrderItem;
import com.bot.models.User;
import com.bot.validators.Validator;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang3.math.NumberUtils;

/**
 *
 * @author tarun
 */
public class OrderController extends HttpServlet implements Constants {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Cart shoppingCart = fetchTheShoppingCart(session);;
        String action = request.getParameter("action");
        String errorMsg = "";
        User user = null;
        
        if (UPDATE_CART.equals(action)) {
            int[] quantity = null;
            String[] productList = request.getParameterValues("productList");
            //String[] quantityStr = request.getParameterValues("quantity");
            String quantityStr;
            if(null == productList /*|| null == quantityStr*/){
                getServletContext().getRequestDispatcher("/CatalogControllerServlet?productCode=xxx").forward(request,response);
            }
            quantity = new int[productList.length];

            for (int i = 0; i < productList.length; i++) {
                quantityStr = request.getParameter(productList[i]);
                if (NumberUtils.isNumber(quantityStr) && !quantityStr.contains(".")) {     // If the quantity value is valid
                    quantity[i] = Integer.parseInt(quantityStr); // Quantity can be zero or any other integer.
                    if(quantity[i] < 0) {
                        quantity[i] = INVALID_QUANTITY;
                    }
                } else if (quantityStr == null || quantityStr.isEmpty()) {
                    quantity[i] = UNSPECIFIED_QUANTITY;   
                                        // No quantity specified, in this case we should increment original
                                        // quantity in cart by 1. This -1 will be used as an indicator that
                                        // no quantity is specified.
                } else {
                    quantity[i] = INVALID_QUANTITY;  
                                        // This -99 is an indicator that the valud entered is invalid
                                        // and hence this item should be ignored and left as it is in
                                        // the cart.
                }
                if (Validator.isValidProductCode(productList[i])) {
                    request.setAttribute(productList[i], quantityStr);
                }
            }

            for (int i = 0; i < productList.length; i++) {
                if (shoppingCart.isProductInCart(productList[i])) {
                    shoppingCart.updateItem(productList[i], quantity[i]);
                } else {
                    shoppingCart.addItem(productList[i]);   // Just in case the product is not already in cart
                }                                           // I don't want customer to go away, I'll add the product
            }                                               // in cart and then she have to buy ;)
            
            getServletContext().getRequestDispatcher("/mycart.jsp").forward(request,response);
        } else if (CHECKOUT.equals(action)) {
            //TODO Code for checkout action
            user = (User) session.getAttribute(USER);
            if(user == null) {
                String fwdString = "/order.jsp";
                session.setAttribute(FWD_STR, fwdString);
                String loginError = "Please login";
                session.setAttribute("loginError", loginError);
                getServletContext().getRequestDispatcher("/signup.jsp").forward(request, response);
                return;
            }
            Order order = fetchCurrentOrder(session, user, shoppingCart.getItemList());
            
            getServletContext().getRequestDispatcher("/order.jsp").forward(request, response);
            return;
        } else if (ADD_TO_CART.equals(action)) {
            String productCode = request.getParameter("productCode");
            if (Validator.isValidProductCode(productCode)) {
                shoppingCart.addItem(productCode);
            }
            System.out.println("*************"+shoppingCart.getCount());
            //getServletContext().getRequestDispatcher("/CatalogControllerServlet?productCode=xxx").forward(request,response);
            //used redirect in order to clear all the parameter which was leading to multiple addition on page refresh
            response.sendRedirect("/biteontime/CatalogControllerServlet");
        } else if (CONFIRM_ORDER.equals(action)) {
            System.out.println("Order Confirmed");
            //Save Order to database
            OrderDB odb = new OrderDB();
            user = fetchTheUser(session);
            Order order = fetchCurrentOrder(session, user, shoppingCart.getItemList());
            order.setTotalCost(shoppingCart.getTotalPrice());
            odb.saveOrder(order);
            clearCurrentSession(session);
            getServletContext().getRequestDispatcher("/invoice.jsp").forward(request,response);
        } else if (VIEW_ORDERS.equals(action)){
            //TODO: add code to fetch and display orders.
            if(isUserInSession(session)){
                OrderDB odb = new OrderDB();
                user = fetchTheUser(session);
                List<Order> orders;
                if(user.getIsAdmin() == 0)
                    orders = odb.getOrderForUser(user);
                else
                    orders = odb.getAllOrders();
                for(Order order: orders) {
                    order.setUser(user);
                }
                session.setAttribute(THE_ORDERS, orders);
                getServletContext().getRequestDispatcher("/orderList.jsp").forward(request, response);
                System.out.println("Orders retrieved");
            } else {
                response.sendRedirect("/biteontime/CatalogControllerServlet");
            }
        } else if (PURCHASE.equals(action)) {
            /*
            1. To purchase a product there should be a user in session.
            2. Use a session variable forwardPage to forward request after login
            3. If user is not in session send him to login page and redirect back
               to accept payment details
            
            */
            user = fetchTheUser(session);
            String forwardURL = "";
            if(user == null) {
                // Implement logic of user not in session later.
            }
            getServletContext().getRequestDispatcher("/payment.jsp").forward(request,response);
        }

    }

    private Cart fetchTheShoppingCart(HttpSession session) {
        Object cart = session.getAttribute(SHOPPING_CART);
        Cart shoppingCart;
        if ((null == cart) || !(cart instanceof Cart)) {
            shoppingCart = new Cart();
            session.setAttribute(SHOPPING_CART, shoppingCart);
        } else {
            shoppingCart = (Cart) cart;
        }
        return shoppingCart;
    }
    
    private User fetchTheUser(HttpSession session) {
        Object userObj = session.getAttribute(USER);
        UserDB userDb = new UserDB();
        User user;
        if((null == userObj) || !(userObj instanceof User)) {
            user = userDb.getOneUser();
            session.setAttribute(USER, user);
        } else {
            user = (User)userObj;
        }
        return user;
    }
    
    private boolean isUserInSession(HttpSession session) {
        User user = (User) session.getAttribute(USER);
        if(user == null) {
            return false;
        } else {
            return true;
        }
    }

    private Order fetchCurrentOrder(HttpSession session, User user,
                            ArrayList<OrderItem> itemList) {
        Order order = (Order) session.getAttribute(CURRENT_ORDER);
        if((order == null) || !(order instanceof Order)){
            order = new Order();
        }
        
        order.setUserId(user.getUserID());
        order.setItemList(itemList);
        session.setAttribute(CURRENT_ORDER, order);
        return order;
    }
    
    private void clearCurrentSession(HttpSession session) {
        session.removeAttribute(CURRENT_ORDER);
        session.removeAttribute(SHOPPING_CART);
    }
}
