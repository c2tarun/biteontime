/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bot.servlets;

import com.bot.util.Util;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author tarun
 */
public class ImageServlet extends HttpServlet {
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String UPLOAD_DIRECTORY = Util.getImageUploadLocation();
        response.setContentType("image/jpeg");
        String fileName = request.getParameter("imageId");
//        UPLOAD_DIRECTORY += System.getProperty("user.home");
//        if(!UPLOAD_DIRECTORY.contains("openshift")){
//            UPLOAD_DIRECTORY += "/NetBeansProjects/images";
//        }
        
        
        File image = new File(UPLOAD_DIRECTORY + File.separator + fileName);
        
        BufferedImage bi = ImageIO.read(image);
        OutputStream out = response.getOutputStream();
        ImageIO.write(bi, "jpg", out);
        out.close();
        
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }
    
}
