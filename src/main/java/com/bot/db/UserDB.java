package com.bot.db;

import com.bot.models.User;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

public class UserDB {

    public List<User> getUser() {
        List<User> users;
        
        EntityManager em = DBUtil.getEmFactory().createEntityManager();
        String query = "SELECT u from User u";
        
        TypedQuery q = em.createQuery(query, User.class);
        
        try {
            users = q.getResultList();
        } finally {
            em.close();
        }
        
        return users;

    }
    
    public User getOneUser() {
        return getUser("1");
    }
    
    public User getUser(String userId) {
        EntityManager em = DBUtil.getEmFactory().createEntityManager();
        User user;
        try {
            user = em.find(User.class, Integer.parseInt(userId));
        } finally {
            em.close();
        }
        
        return user;
    }
    
    public void addUser(String firstName, String lastName, String emailAddress,
                    String addressField1, String addressField2, String city, String state){
        User user = new User(firstName, lastName, emailAddress, addressField1, 
                addressField2, city, state, state, city, lastName);
        addUser(user);
    }
    
    public void addUser(User user){
        insertUser(user);
    }

    public static void insertUser(User user) {
        EntityManager em = DBUtil.getEmFactory().createEntityManager();
        EntityTransaction transaction = em.getTransaction();
        try{
            transaction.begin();
            em.persist(user);
            transaction.commit();
        } catch(Exception e){
            e.printStackTrace();
            transaction.rollback();
        } finally {
            em.close();
        }
    }
    
    public User getUserByEmail (String email) throws NoResultException{
        EntityManager em = DBUtil.getEmFactory().createEntityManager();
        EntityTransaction transaction = em.getTransaction();
        String query = "SELECT u from User u WHERE u.emailAddress = :email";
        TypedQuery<User> q = em.createQuery(query, User.class);
        q.setParameter("email", email);
        
        User user;
        try {
            user = q.getSingleResult();
        } finally {
            em.close();
        }
        return user;
    }
    
    public User getUserByUserID (int userID) {
        EntityManager em = DBUtil.getEmFactory().createEntityManager();
        EntityTransaction transaction = em.getTransaction();
        User user;
        try {
            user = em.find(User.class, userID);
        } finally {
            em.close();
        }
        return user;
    }
    
}
