/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bot.db;

import com.bot.models.Order;
import com.bot.models.User;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;

/**
 *
 * @author tarun
 */
public class OrderDB {

    public void saveOrder(Order order) {
        EntityManager em = DBUtil.getEmFactory().createEntityManager();
        EntityTransaction transaction = em.getTransaction();
        try {
            transaction.begin();
            em.persist(order);
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            transaction.rollback();
        } finally {
            em.close();
        }
    }
    
    public List<Order> getOrderForUser(User user) {
        List<Order> orders;
        
        EntityManager em = DBUtil.getEmFactory().createEntityManager();
        String qString = "SELECT o from Order o where o.userId = :userId";
        
        TypedQuery<Order> q = em.createQuery(qString, Order.class);
        q.setParameter("userId", user.getUserID());
        
        try {
            orders = q.getResultList();
        } finally {
            em.close();
        }
        
        return orders;
    }
    
    public List<Order> getAllOrders() {
        List<Order> orders;
        
        EntityManager em = DBUtil.getEmFactory().createEntityManager();
        String qString = "SELECT o from Order o";
        
        TypedQuery<Order> q = em.createQuery(qString, Order.class);
        
        try {
            orders = q.getResultList();
        } finally {
            em.close();
        }
        
        return orders;
    }

}
