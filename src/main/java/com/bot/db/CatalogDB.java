package com.bot.db;

import com.bot.models.Catalog;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;

public class CatalogDB {

    public List<Catalog> getCatalogs() {
        List<Catalog> catalog;
        
        EntityManager em = DBUtil.getEmFactory().createEntityManager();
        String query = "SELECT c from Catalog c";
        
        TypedQuery q = em.createQuery(query, Catalog.class);
        
        try {
            catalog = q.getResultList();
        } finally {
            em.close();
        }
        
        return catalog;

    }
    
    public Catalog getCatalog(String catalogID) {
        EntityManager em = DBUtil.getEmFactory().createEntityManager();
        Catalog catalog;
        try {
            catalog = em.find(Catalog.class, Integer.parseInt(catalogID));
        } finally {
            em.close();
        }
        
        return catalog;
    }
    
    public static void insertCatalog(Catalog catalog) {
        EntityManager em = DBUtil.getEmFactory().createEntityManager();
        EntityTransaction transaction = em.getTransaction();
        try{
            transaction.begin();
            em.persist(catalog);
            transaction.commit();
        } catch(Exception e){
            transaction.rollback();
        } finally {
            em.close();
        }
    }
}
