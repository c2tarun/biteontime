package com.bot.db;

import com.bot.models.Product;
import java.sql.SQLException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;
import org.eclipse.persistence.exceptions.DatabaseException;

/**
 * This class will fetch details from DB and return list of products.
 */
public class ProductDB {

    public List<Product> getProducts() {
        List<Product> products;

        EntityManager em = DBUtil.getEmFactory().createEntityManager();
        String qString = "SELECT p from Product p";

        TypedQuery<Product> tQuery = em.createQuery(qString, Product.class);

        try {
            products = tQuery.getResultList();
        } finally {
            em.close();
        }

        return products;
    }

    public Product getProductByProductCode(String productCode) {

        Product product;
        EntityManager em = DBUtil.getEmFactory().createEntityManager();

        try {
            product = em.find(Product.class, productCode);
        } finally {
            em.close();
        }

        return product;
    }

    public List<Product> getProductsByCategory(String category) {
        List<Product> products;
        EntityManager em = DBUtil.getEmFactory().createEntityManager();
        String query;
        if ("all".equals(category)) {
            return getProducts();
        } else {
            query = "SELECT p from Product p WHERE p.category = :category";
        }

        TypedQuery<Product> q = em.createQuery(query, Product.class);
        q.setParameter("category", category);

        try {
            products = q.getResultList();
        } finally {
            em.close();
        }

        return products;
    }

    public void addProduct(String productCode, String productName,
            String catalogCategory, double price, String description, String imageUrl) throws DatabaseException {
        addProduct(new Product(productCode, productName, catalogCategory, description, price, imageUrl));
    }

    public void addProduct(Product product) throws DatabaseException{
        insertProduct(product);
    }

    public static void insertProduct(Product product) throws DatabaseException {
        EntityManager em = DBUtil.getEmFactory().createEntityManager();
        EntityTransaction transaction = em.getTransaction();
        transaction.begin();
        em.persist(product);
        transaction.commit();
        em.close();
    }
}
