/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bot.db;

/**
 *
 * @author tarun
 */
public interface Constants {
    // Constants for cart
    public static final String UPDATE_CART = "updateCart";
    public static final String CHECKOUT = "checkout";
    public static final String ADD_TO_CART = "addToCart";
    public static final String CONFIRM_ORDER = "confirmOrder";
    public static final String VIEW_ORDERS = "viewOrders";
    public static final String PURCHASE = "purchase";
    
    public static final int UNSPECIFIED_QUANTITY = -1;
    public static final int INVALID_QUANTITY = -99;
    
    // Session contants
    public static final String SHOPPING_CART = "theShoppingCart";
    public static final String USER = "theUser";
    public static final String CURRENT_ORDER = "currentOrder";
    public static final String THE_ORDERS = "theOrders";
    public static final String FWD_STR = "fwdStr";
    
    public static final String IMAGE_SERVLET = "ImageServlet?imageId=";
}
