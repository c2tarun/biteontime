/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bot.util;

import java.io.File;

/**
 *
 * @author tarun
 */
public class Util {

    public static String getImageUploadLocation() {
        String home = System.getProperty("user.home");
        String uploadLocation = "";
        if (home != null) {
            if (home.contains("openshift")) {
                uploadLocation += home + File.separator + "jbossews" + 
                        File.separator + "biteontime" + 
                        File.separator + "images";
            } else {
                uploadLocation += home + File.separator + "biteontime" + 
                        File.separator + "images";
            }
        }
        return uploadLocation;
    }
}
