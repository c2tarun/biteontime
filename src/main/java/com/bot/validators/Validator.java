/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bot.validators;

import com.bot.models.Product;
import java.io.File;
import java.util.regex.Pattern;
import org.apache.commons.lang3.math.NumberUtils;

/**
 *
 * @author tarun
 */
public class Validator {
    public static boolean isValidProductCode(String productCode) {
        final Pattern pattern = Pattern.compile("CODE[0-9][0-9][0-9]");
        return pattern.matcher(productCode).matches();
    }
    
    public static String validateProduct(Product product) {
        String errors = "";
        if(!isValidProductCode(product.getProductCode())) {
            errors += "Product Code invalid. Format CODE###<br>";
        }
        if(product.getProductName() == null || product.getProductName().length() <= 0){
            errors += "Product Name missing<br>";
        }
        if(product.getCategory() == null || product.getCategory().length() <= 0){
            errors += "Catalog Category missing<br>";
        }
        if(!validPrice(product.getPrice())) {
            errors += "Price is invalid<br>";
        }
        if(product.getDescription() == null || product.getDescription().length() <= 0){
            errors += "Description missing<br>";
        }
        return errors;
    }
    
    public static boolean validateImage(File image){
        if(image.length() <= 0){
            return false;
        } else {
            return true;
        }
    }
    
    public static boolean validPrice(double price) {
        return NumberUtils.isNumber(price + "");
    }
}
