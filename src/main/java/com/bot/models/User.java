package com.bot.models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * This model class will store the details of Users.
 */

@Entity
@Table (name = "User")
public class User implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int userID;
    private String firstName;
    private String lastName;
    private String emailAddress;
    private String addressField1;
    private String addressField2;
    private String city;
    private String state;
    
    @Column(name = "zipPostalCode")
    private String postCode;
    private String country;
    private String password;
    private int isAdmin;
    
//zero argument constructor
    
    public User() {
        
        firstName = null;
        lastName = null;
        emailAddress = null;
        addressField1 = null;
        addressField2 = null;
        city = null;
        state = null;
        postCode = null;
        country = null; 
    }
            
  // constructor to initialize a user.
    
    public User(String firstName, String lastName, String emailAddress, 
            String addressField1, String addressField2, String city, 
            String state, String postCode, String country, String password)  {
        this.firstName = firstName;
        this.lastName = lastName;
        this.emailAddress = emailAddress;
        this.addressField1 = addressField1;
        this.addressField2 = addressField2;
        this.city = city;
        this.state = state;
        this.postCode = postCode;
        this.country = country;
        this.password = password;
     }

    public int getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(int isAdmin) {
        this.isAdmin = isAdmin;
    }

    
    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }
    
    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public String getAddressField1() {
        return addressField1;
    }

    public String getAddressField2() {
        return addressField2;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }
    
    public String getPostCode() {
        return postCode ;
    }

    public String getCountry() {
        return country;
    }
    
    
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public void setAddressField1(String addressField1) {
        this.addressField1 = addressField1;
    }


    public void setAddressField2(String addressField2) {
        this.addressField2 = addressField2;
    }

    public void setCity(String city) {
        this.city = city;
    }
    
     public void setState(String state) {
        this.state = state;
    }

    
    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    
}
