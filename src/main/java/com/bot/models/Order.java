package com.bot.models;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * This model class will store the details of the orders.
 */

@Entity
@Table (name = "OrderDesc")
public class Order implements Serializable{
    
    @Id
    @GeneratedValue (strategy = GenerationType.AUTO)
    private int orderNumber;
    private Date date;
    @Transient
    private User user;
    private int userId;
    
    @OneToMany (cascade = CascadeType.ALL)
    private Collection<OrderItem> itemList = new ArrayList<OrderItem>(); 
    private float taxRates;
    private double totalCost;
    @Column (name = "paid")
    private boolean isPaid;
    
//zero argument constructor
    
    public Order() {
        Calendar cal = Calendar.getInstance();
        date = new Date(cal.getTimeInMillis());
        itemList = new ArrayList<OrderItem>();
        taxRates = 10.0f;
        totalCost = 0.0d;
        isPaid = false;
    }
            
  // constructor to initialize order details.
    
    public Order(Date date, int userId, ArrayList<OrderItem> itemList, 
            float taxRates, double totalCost, boolean isPaid)  {
        
        this.date = date;
        this.userId = userId;
        this.user = user;
        this.itemList = itemList;
        this.taxRates = taxRates;
        this.totalCost = totalCost;
        this.isPaid = isPaid;

     }
    
    public Date getDate() {
        
        return date;
    }
    
    public User getUser() {
        
        return user;
    }

    public ArrayList<OrderItem> getItemList() {
        return (ArrayList<OrderItem>) itemList;
    }

    public void setItemList(ArrayList<OrderItem> itemList) {
        this.itemList = itemList;
    }
    
    public double getTotalCost() {
        
        return totalCost;
    }

    public float getTaxRates() {
        
        return taxRates;
    }
    
    
    public boolean getPaid() {
        
        return isPaid;
    }   
    
    public void setDate(Date date) {
        this.date = date;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setTotalCost(double totalCost) {
        this.totalCost = totalCost;
    }
    
    public void setTaxRates(float taxRates) {
        this.taxRates = taxRates;
    }    

    public void setIsPaid(boolean isPaid) {
        this.isPaid = isPaid;
    }

    public int getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
    
    
    
    public double getTotalPrice() {
        double totalPrice = 0;
        for (OrderItem item : itemList) {
            totalPrice += item.getQuantity() * item.getProduct().getPrice();
        }
        
        return totalPrice;
    }
    
    public double getTotalTax() {
        return (taxRates/100)*getTotalPrice();
    }
}
