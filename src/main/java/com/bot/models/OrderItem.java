package com.bot.models;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * Details of the Ordered Items { product, quantity and the total cost}
 */
@Entity
@Table (name = "OrderItem")
public class OrderItem implements Serializable {

    @Id
    @GeneratedValue (strategy = GenerationType.AUTO)
    private int itemNumber;
    @Transient
    private Product product;
    
    private String productCode;
    private int quantity;

    //zero arguments Constructor 
    public OrderItem() {

        product = new Product();
        quantity = 0;

    }
    
    public OrderItem(Product product, int quantity) {

        this.product = product;
        this.quantity = quantity;
        this.productCode = product.getProductCode();

    }

    public int getItemNumber() {
        return itemNumber;
    }

    public void setItemNumber(int itemNumber) {
        this.itemNumber = itemNumber;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }
    
    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    
    

    //getTotal() will calculate the total 
    private double getTotal() {

        double total = 0.0d;
        if (quantity == 0) {
            return 0.0d;
        }
        total = product.getPrice() * quantity;
        return total;
    }

}
