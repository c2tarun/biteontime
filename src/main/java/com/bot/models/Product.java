package com.bot.models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * This model class will store the details individual products.
 */
@Entity
@Table (name = "Product")
public class Product implements Serializable{
    
    @Id
    private String productCode;
    private String productName;
    
    @Column (name = "catalogCategory")
    private String category;
    private String description;
    private double price;
    
    @Column (name = "imageUrl")
    private String imgURL;
    
    // zero-argument constructor
    public Product() {
        productCode = null;
        productName = null;
        category = null;
        description = null;
        price = 0.0;
        imgURL = null;
    }
    
    // constructor to initialize a product.
    public Product(String productCode, String productName, String category, 
            String description, double price, String imgURL ){
        this.productCode = productCode;
        this.productName = productName;
        this.category = category;
        this.description = description;
        this.price = price;
        this.imgURL = imgURL;
    }

    public String getProductCode() {
        return productCode;
    }

    public String getProductName() {
        return productName;
    }

    public String getCategory() {
        return category;
    }

    public String getDescription() {
        return description;
    }

    public double getPrice() {
        return price;
    }

    public String getImgURL() {
        return imgURL;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setImgURL(String imgURL) {
        this.imgURL = imgURL;
    }

    public void Search(String category){
        if(this.category==category){
            
        }
            
    }
            
    
    
}
