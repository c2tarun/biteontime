package com.bot.models;

import com.bot.db.Constants;
import com.bot.db.ProductDB;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * This model class stores the Cart
 */
public class Cart implements Serializable, Constants {

    // Elements in Cart
    private ArrayList<OrderItem> itemList;

    // zero argument constructor
    public Cart() {
        itemList = new ArrayList<OrderItem>();
    }

    public ArrayList<OrderItem> getItemList() {
        return itemList;
    }

    public void setItemList(ArrayList<OrderItem> itemList) {
        this.itemList = itemList;
    }

    /**
     * Add itemToAdd to current cart. If item exists, its quantity will
     * be increased by 1
     * @param itemToAdd 
     */
    public void addItem(OrderItem itemToAdd) {
        for (OrderItem item : itemList) {
            if (item.getProduct().getProductCode().equals(itemToAdd.getProduct().getProductCode())) {
                item.setQuantity(item.getQuantity() + 1);
                return;
            }
        }
        itemList.add(itemToAdd);
    }
    
    /**
     * Overloaded for ProductCode
     * If item exists, its quantity will be increased by 1
     * Otherwise item will be added with default quantity 1
     * @param productCode 
     */
    public void addItem(String productCode) {
        for (OrderItem item : itemList) {
            if (item.getProduct().getProductCode().equals(productCode)) {
                item.setQuantity(item.getQuantity() + 1);
                return;
            }
        }
        ProductDB pdb = new ProductDB();
        Product productToAdd = pdb.getProductByProductCode(productCode);
        OrderItem itemToAdd = new OrderItem(productToAdd, 1);
        itemList.add(itemToAdd);
    }
    
    
    /**
     * Update item in cart with the quantity mentioned.
     * @param productCode
     * @param quantity 
     */
    public void updateItem(String productCode, int quantity) {
        for (OrderItem item : itemList) {
            if (item.getProduct().getProductCode().equals(productCode)) {
                if(quantity == 0){      // If quantity is zero while updating
                    removeItem(item);   // then remove the item from cart.
                    break;
                } else if (quantity == UNSPECIFIED_QUANTITY) {
                    addItem(productCode);   // read the description of addItem
                } else if (quantity == INVALID_QUANTITY) {
                    // In case of Invalid quantity don't do anything, just ignore
                } else {
                    item.setQuantity(quantity); // In case if the quantity is an perfect integer
                    break;                      // the overwrite previous quantity with new.
                }
            }
        }
    }

    /**
     * Remove itemToRemove from current cart
     * @param itemToRemove 
     */
    public void removeItem(OrderItem itemToRemove) {
        for (OrderItem item : itemList) {
            if (item.getProduct().getProductCode().equals(itemToRemove.getProduct().getProductCode())) {
                itemList.remove(item);
                return;
            }
        }
    }
    
    /**
     * This method will check if passed product in cart or not.
     * @param productCode
     * @return 
     */
    public boolean isProductInCart(String productCode) {
        for (OrderItem item : itemList) {
            if(item.getProduct().getProductCode().equals(productCode)) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * This method is same is getItem, only change is in the name of the method
     *
     * @return
     */
    public ArrayList<OrderItem> getItem() {
        return itemList;
    }
    
    /**
     * Clears entire cart's content
     */
    public void emptyCart() {
        itemList.removeAll(itemList);
    }
    
    public int getCount() {
        return itemList.size();
    }
    
    public double getTotalPrice() {
        double totalPrice = 0;
        for (OrderItem item : itemList) {
            totalPrice += item.getQuantity() * item.getProduct().getPrice();
        }
        
        return totalPrice;
    }
    
}
