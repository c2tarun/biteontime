<!DOCTYPE html>
<html>
<head>
    <title>Bite on Time</title>
<!--    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">-->
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="css/catalog.css">
    <link rel="stylesheet" type="text/css" href="css/product.css">
</head>
<body>
    <div id="super">

        <%@ include file="header.jsp" %>


        <%@ include file="user-navigation.jsp" %>
        

        <div id="main_page">

            <%@ include file="site-navigation.jsp" %>

            <div>
                <form action="CatalogControllerServlet" method="post">
                    <select name="category">
                        <option value="beverages">Beverage</option>
                        <option value="bakery">Bakery</option>                    
                    </select>
                    <input type="submit" value="Apply Filter">
                </form>
            </div>

            <div id="description">
                <ol>
                    <li>                                                 
                        <table>
                            <tr>
                                <th rowspan="2"><a href="product.jsp"><img src="images/vanilla_IC_SB.jpg" alt="vanila Ice Cream"></a></th>
                                <th colspan="2" class="pro_description"><a href="product.jsp">Starbucks Vanilla Iced Coffee</a></th>
                            </tr>
                            <tr>
                                <td>Nutrition: 100 Cal</td>
                                <td class = "price">
                                    <table>
                                        <tr>
                                            <td>$7</td>
                                            <td><a href="#"><img src="images/add-to-cart-icon.jpg" alt="add to card"></a></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table></li>
                        <li>
                            <table>
                                <tr>
                                    <th rowspan="2"><a href="#"><img src="images/HamSwissPanini_SB.jpg" alt="Ham Swiss Panini"></a></th>
                                    <th colspan="2" class="pro_description"><a href="#">Ham and Swiss cheese with Dijon mustard on a focaccia roll.</a></th>
                                </tr>
                                <tr>
                                    <td>Nutrition: 200 Cal</td>
                                    <td class = "price">
                                        <table>
                                            <tr>
                                                <td>$15</td>
                                                <td><a href="#"><img src="images/add-to-cart-icon.jpg" alt="add to cart"></a></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table></li>
                            <li>
                                <table>
                                    <tr>
                                        <th rowspan="2"><a href="#"><img src="images/ChocoCakePop_SB.jpg" alt="Chocolate cake"></a></th>
                                        <th colspan="2" class="pro_description"><a href="#">Chocolate cake mixed with chocolate butter cream, dipped in dark chocolate and topped with sugar sprinkles.</a></th>
                                    </tr>
                                    <tr>
                                        <td>Nutrition: 250 Cal</td>
                                        <td class = "price">
                                            <table>
                                                <tr>
                                                    <td>$10</td>
                                                    <td><a href="#"><img src="images/add-to-cart-icon.jpg" alt="add to cart"></a></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table></li>
                            </ol>
                            
                        </div>
                    </div>


                    <%@ include file="footer.jsp" %>
                </div>
            </body>
            </html>