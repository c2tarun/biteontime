<!DOCTYPE html>
<html>
    <head>
        <title>Bite on Time</title>
<!--        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">-->
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <link rel="stylesheet" type="text/css" href="css/product.css">
        <link rel="stylesheet" type="text/css" href="css/buttons.css">
    </head>
    <body>
    <div id="super">
<%@ include file="header.jsp" %>


       <%@ include file="user-navigation.jsp" %>
        

        <div id="main_page">

            <%@ include file="site-navigation.jsp" %>
            <div id="description">
                <div id="breadcrumb">
                    <p><a href="index.jsp">Home</a> >> <a href="CatalogControllerServlet">Catalog</a> >> <a href="CatalogControllerServlet?productCode=${product.productCode}">Product</a> </p>
                </div>
                <h3>${product.productName}</h3>
                    <table>
                        <tr>
                            <td rowspan="2"><img class="product_image" src="${product.imgURL}" alt="${product.description}"></td>
                            <td class="align_right">${product.price} USD</td>
                        </tr>
                        <tr>
                            <td class="align_right"><a href="OrderController?productCode=${product.productCode}&amp;action=addToCart"><img src="images/add-to-cart-icon.jpg" alt="add to card"></a></td>
                        </tr>
                    </table>
                    <br><br><br>

                    <table>
                        <tr>
                        <th>Nutrition Table</th>
                        </tr>
                        <tr>
                            <td>Calories</td>
                            <td>100</td>
                        </tr>
                        <tr>
                            <td>Total Fat</td>
                            <td>.5gm</td>
                        </tr> 

                    </table>

                    <br><br><br><br><br><br><br><br><br><br>
                    <a href="CatalogControllerServlet" class="button">back</a>
                
            </div>
         </div>


<%@ include file="footer.jsp" %>
    </div>
    </body>
</html>