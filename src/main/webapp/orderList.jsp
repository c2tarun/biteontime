<!DOCTYPE html>
<html>
    <head>
        <title>Bite on Time</title>
<!--        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">-->
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <link rel="stylesheet" type="text/css" href="css/order.css">
        <link rel="stylesheet" type="text/css" href="css/buttons.css">
    </head>
    <body>
    <div id="super">
    <%@ include file="header.jsp" %>
        <%@ include file="user-navigation.jsp" %>
        <div id="main_page">

            <%@ include file="site-navigation.jsp" %>  
            <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
            <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
            
            <div id="description">
                <div id="breadcrumb">
                    <p><a href="index.jsp">Home</a> >> <a href="order.jsp">Order</a> </p>
                </div>

                 <table class="order_table">
                    <tr>
                        <th class="item_col">Order Number</th>
                        <th class="item_col">Customer</th>
                        <th class="item_col">Order Date</th>
                        <th class="item_col">Total</th>                        
                    </tr>
                    <c:forEach var="order" items="${theOrders}">
                    <tr>
                        <td>${order.orderNumber}</td>
                        <td>${order.user.firstName}</td>
                        <td><fmt:formatDate type="date" value="${order.date}" /></td>
                        <td>${order.totalCost}</td>
                    </tr>
                    </c:forEach>
                 </table>  
                
            </div> 
        </div>
<%@ include file="footer.jsp" %>
    </div>
    </body>
</html>