<!DOCTYPE html>
<html>
    <head>
        <title>Bite on Time</title>
<!--        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">-->
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <link rel="stylesheet" type="text/css" href="css/mycart.css">
        <link rel="stylesheet" type="text/css" href="css/buttons.css">
    </head>
    <body>
        <div id="super">
            <%@ include file="header.jsp" %>

            <%@ include file="user-navigation.jsp" %>

            <%@ include file="site-navigation.jsp" %>
            
            <div id="main_page">
                <p>${message}</p>
                <form action="DBController" method="POST" enctype="multipart/form-data">
                    <table>
                        <tr><td>Product Code</td><td><input type="text" name="productCode" value="${productCode}" /></td></tr>
                        <tr><td>Product Name</td><td><input type="text" name="productName" value="${productName}"/></td></tr>
                        <tr><td>Catalog Category</td>
                            <td>
                                <select name="catalog" class="style_selector">
                                <c:forEach var="catalog" items="${catalogs}" >
                                <option value="${catalog.catalogName}">${catalog.catalogName}</option>
                                </c:forEach>
                                </select>
                                
                                <a href="insertCatalog.jsp">Add Catalog</a>
                            </td>
                        </tr>
                        <tr><td>Price</td><td><input type="number" name="price" value="${price}"/></td></tr>
                        <tr><td>Description</td><td><input type="text" name="description" value="${description}"/></td></tr>
                        <tr><td>Upload Image</td><td><input type="file" name="image" /></td></tr>
                        <tr><td></td><td><button type="submit" name="action" value="addProductToDB" class="button">Add to DB</button></td></tr>
                    </table>
                    
                </form>
            </div>


            <%@ include file="footer.jsp" %>
        </div>
    </body>
</html>