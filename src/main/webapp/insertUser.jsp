<!DOCTYPE html>
<html>
    <head>
        <title>Bite on Time</title>
<!--        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">-->
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <link rel="stylesheet" type="text/css" href="css/mycart.css">
        <link rel="stylesheet" type="text/css" href="css/buttons.css">
    </head>
    <body>
        <div id="super">
            <%@ include file="header.jsp" %>

            <%@ include file="user-navigation.jsp" %>

            <%@ include file="site-navigation.jsp" %>
            
            <div id="main_page">
                <p>${message}</p>
                <form action="DBController" method="GET">
                    <table>
                        <tr><td>First Name</td><td><input type="text" name="firstName" /></td></tr>
                        <tr><td>Last Name</td><td><input type="text" name="lastName" /></td></tr>
                        <tr><td>Email</td><td><input type="text" name="emailAddress" /></td></tr>
                        <tr><td>Address Line 1</td><td><input type="text" name="addr1" /></td></tr>
                        <tr><td>Address Line 2</td><td><input type="text" name="addr2" /></td></tr>
                        <tr><td>City</td><td><input type="text" name="city" /></td></tr>
                        <tr><td>State</td><td><input type="text" name="state" /></td></tr>
                        <tr><td>Zip</td><td><input type="text" name="zip" /></td></tr>
                        <tr><td>Country</td><td><input type="text" name="country" /></td></tr>
                        <tr><td>Password</td><td><input type="text" name="password" /></td></tr>
                        <tr><td></td><td><button type="submit" name="action" value="addUserToDB" class="button">Add to DB</button></td></tr>
                    </table>
                    
                </form>
            </div>


            <%@ include file="footer.jsp" %>
        </div>
    </body>
</html>