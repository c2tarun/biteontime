<!DOCTYPE html>
<html>
    <head>
        <title>Bite on Time</title>
<!--        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">-->
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <link rel="stylesheet" type="text/css" href="css/mycart.css">
        <link rel="stylesheet" type="text/css" href="css/buttons.css">
    </head>
    <body>
        <div id="super">
            <%@ include file="header.jsp" %>

            <%@ include file="user-navigation.jsp" %>

            <%@ include file="site-navigation.jsp" %>
            
            <div id="main_page">
                <p>${message}</p>
                <form action="DBController" method="GET" enctype="multipart/form-data">
                    <table>
                        <tr><td>Catalog Name</td><td><input type="text" name="catalogName" value="${catalogName}" /></td></tr>
                        <tr><td></td><td><button type="submit" name="action" value="addCatalogToDB" class="button">Add to DB</button></td></tr>
                    </table>
                    <a href="insertProduct.jsp" class="button">Back</a>                    
                </form>
            </div>


            <%@ include file="footer.jsp" %>
        </div>
    </body>
</html>