<!DOCTYPE html>
<html>
    <head>
        <title>Bite on Time</title>
        <!--	<meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">-->
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <link rel="stylesheet" type="text/css" href="css/buttons.css">        
    </head>
    <body>
        <div id="super">
            <%@ include file="header.jsp" %>
            <%@ include file="user-navigation.jsp" %>
            <div id="main_page">
                <%@ include file="site-navigation.jsp" %>
                <div id="description">
                    <div id="breadcrumb">
                        <p><a href="index.jsp">Home</a> >> <a href="signup.jsp">Sign Up</a> </p>
                    </div>
                    <form action="OrderController" method="POST">
                        <h3>Enter your payment details here</h3>
                        <fieldset>
                            <table>
                                <tr>
                                    <td>Credit Card Type<td>
                                        <select name="Card" size="1">
                                            <option>Visa</option>
                                            <option>Master Card</option>
                                            <option>Discover</option>
                                            <option>Paypal</option>                                                                                    
                                        </select>
                                    </td>                                                                        
                                </tr>                        
                                <tr>
                                    <td>Card Number</td>
                                    <td><input type="text" name="t7" size="16" required title="Invalid Card Number" pattern="[0-9]{16}"/></td>
                                </tr>
                                <tr>
                                    <td>CardHolder's Name</td>
                                    <td><input type="text" name="t7" size="16" required title="Only Alphabets" pattern="^[a-zA-Z]{6,30}$"/></td>
                                </tr>                                                        

                                <tr>
                                    <td>Expiration Date(MM/YYYY)</td>
                                    <td>
                                        <select name="M" size="1" required>
                                            <option value="Jan">Jan</option>
                                            <option value="Feb">Feb</option>
                                            <option value="Mar">Mar</option>
                                            <option value="Apr">Apr</option>
                                            <option value="May">May</option>
                                            <option value="Jun">Jun</option>
                                            <option value="Jul">Jul</option>
                                            <option value="Aug">Aug</option>
                                            <option value="Sep">Sep</option>
                                            <option value="Oct">Oct</option>
                                            <option value="Nov">Nov</option>
                                            <option value="Dec">Dec</option>
                                        </select>
                                    </td>
                                    <td>							<td>
                                        <select name="Y" size="1" required=>
                                            <option  value="2014">2014</option>
                                            <option value="2015">2015</option>
                                            <option value="2016">2016</option>
                                            <option value="2017">2017</option>
                                            <option value="2018">2018</option>
                                            <option value="2019">2019</option>
                                            <option value="2020">2020</option>
                                        </select>
                                    </td>                                                                        
                                </tr>
                                <tr>

                                    <td>CVV (3 Digit)</td>
                                    <td><input type="text" size="3" name="t6" required title="Invalid CVV Length" pattern="[0-9]{3}"/></td>
                                </tr>
                            </table>
                        </fieldset>
                        <h4>Your card will be charged a total amount of: ${currentOrder.totalPrice + currentOrder.totalTax}</h4>
                        <button type="submit" name="action" value="confirmOrder" class="button_thin">Confirm Order</button>
                    </form>                            
                </div>
            </div>
            <%@ include file="footer.jsp" %>
        </div>
    </body>
</html>