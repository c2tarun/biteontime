        <header>
            <img src="images/logo.png" alt="banner">
            <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
            <c:if test="${theUser eq null}">
            <p>
                Not Signed in, <a href="signup.jsp">Sign in</a><br><br>
                <a href="signup.jsp">Sign Up</a>
            </p>
            </c:if>
            <c:if test="${theUser ne null}">
                <p>
                    Welcome, ${theUser.firstName} <br><br>
                <a href="LoginController?action=logout" >Logout</a>
                </p>
                
            </c:if>
            
        </header>
