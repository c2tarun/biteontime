<!DOCTYPE html>
<html>
    <head>
        <title>Bite on Time</title>
        <link rel="stylesheet" type="text/css" href="css/main.css">
    </head>
    <body>
        <div id="super">
            <%@ include file="header.jsp" %>
            <%@ include file="user-navigation.jsp" %>


            <div id="main_page">

                <%@ include file="site-navigation.jsp" %>


                <div id="description">
                    <div id="breadcrumb">
                        <p><a href="index.jsp">Home</a> >> <a href="signup.jsp">Sign Up</a> </p>
                    </div>

                    <h3>Already have an Account?? </h3>


                    <div class="error">${loginError}</div><br>
                    <form action="LoginController">
                        <fieldset>
                            <legend><b> Login </b></legend>
                            Email Address <input type="text" size="20" name="email"/>	<br/>
                            Password <input type="password" size="20" name="password"/>	<br/>
                            <table>
                                <tr>
                                    <td><input type="checkbox" />Remember me</td>
                                    <td> <input type="submit" value="Submit"/></td>
                                    <td><input type="reset" value="Reset"/></td>
                                </tr>
                            </table>
                        </fieldset>
                    </form>
                    <h3>Sign-up for free and get exclusive offers </h3>
                    <div class="error">${signUpError}</div><br>
                    <form action="DBController">
                        <fieldset>
                            <legend><b> Registration Form </b></legend>
                            <table>
                                <tr>
                                    <td>First Name</td>
                                    <td><input type="text" name="firstName" size="20" value="${firstName}" name="t3"/></td>
                                </tr>
                                <tr>
                                    <td>Last Name</td>
                                    <td><input type="text" name="lastName" size="20" value="${lastName}" name="t4"/></td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td><input type="text" name="emailAddress" size="20" value="${emailAddress}" name="t4"/></td>
                                </tr>

                                <tr>
                                    <td>Password</td>
                                    <td><input type="password" name="password" size="20" name="t5"/></td>
                                </tr>
                                <tr>

                                    <td>Confirm Password</td>
                                    <td><input type="password" name="confirmPassword" size="20" name="t6"/></td>
                                </tr>

                                <tr>
                                    <td>Address Field1</td>
                                    <td><input type="text" name="addr1" value="${addr1}" /></td>
                                </tr>
                                
                                <tr>
                                    <td>Address Field2</td>
                                    <td><input type="text" name="addr2" value="${addr2}" /></td>
                                </tr>
                                
                                <tr>
                                    <td>City</td>
                                    <td><input type="text" name="city" value="${city}"/></td>
                                </tr>
                                
                                <tr>
                                    <td>State</td>
                                    <td><input type="text" name="state" value="${state}" /></td>
                                </tr>
                                
                                <tr>
                                    <td>Zip Code</td>
                                    <td><input type="text" name="zip" size="20" name="t8" value="${zip}" /></td> 
                                </tr>
                                
                                <tr>
                                    <td>Country</td>
                                    <td><input type="text" name="country" value="${country}" /></td>
                                </tr>

                                <tr>
                                    <td><button type="submit" name="action" value="addUserToDB" class="button">Submit</button></td>
                                    <td> <input type="reset" value="Reset"/></td>
                                </tr>
                            </table>
                        </fieldset>
                    </form>







                </div>
            </div>
            <%@ include file="footer.jsp" %>
        </div>
    </body>
</html>