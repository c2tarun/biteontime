<!DOCTYPE html>
<html>
    <head>
        <title>Bite on Time</title>
<!--        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">-->
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <link rel="stylesheet" type="text/css" href="css/order.css">
        <link rel="stylesheet" type="text/css" href="css/buttons.css">
    </head>
    <body>
    <div id="super">
    <%@ include file="header.jsp" %>
        <%@ include file="user-navigation.jsp" %>
        <div id="main_page">

            <%@ include file="site-navigation.jsp" %>  
            <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
            <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
            
            <div id="description">
                <div id="breadcrumb">
                    <p><a href="index.jsp">Home</a> >> <a href="order.jsp">Order</a> </p>
                </div>
                <h3>Invoice</h3>
        
                <h1>Paid in Full.</h1>
            </div> 
        </div>
<%@ include file="footer.jsp" %>
    </div>
    </body>
</html>