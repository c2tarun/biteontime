<!DOCTYPE html>
<html>
    <head>
        <title>Bite on Time</title>
<!--        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">-->
        <link rel="stylesheet" type="text/css" href="css/main.css">
    </head>
    <body>
    <div id="super">
<%@ include file="header.jsp" %>
        <%@ include file="user-navigation.jsp" %>
        

        <div id="main_page">

            <%@ include file="site-navigation.jsp" %>
            <div id="description">
                <p id="goal">
                    <u>Our Goal</u>: To give you food when you need it most, where nobody else is going to deliver, in a way that makes you feel safe :)
                </p>
                <ul id="scenarios">
                    <li>Need to catch a bus in 30 mins and no time to buy breakfast.</li>
                    <li>Have a lecture in 30 min and no time to buy snack.</li>
                    <li>Passing through a predecided place and need snack from your favorite joint.</li>
                    <li>Need to go somewhere important and don't have time to buy food.</li>
                </ul>                
                <p>If you are in any of the above situations, Call Us. We'll deliver your food to any place you want not just at your home. Bus stop, College, Railway Station etc, anywhere you want food and don't have time to buy it, call us. We'll be ready with your food at your destination on your arrival :)</p>

            </div>
         </div>

<%@ include file="footer.jsp" %>
    </div>
    </body>
</html>
         
