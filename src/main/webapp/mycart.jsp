<!DOCTYPE html>
<html>
    <head>
        <title>Bite on Time</title>
<!--        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">-->
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <link rel="stylesheet" type="text/css" href="css/mycart.css">
        <link rel="stylesheet" type="text/css" href="css/buttons.css">
    </head>
    <body>
    <div id="super">
<%@ include file="header.jsp" %>

<%@ include file="user-navigation.jsp" %>
        

        <div id="main_page">

            <%@ include file="site-navigation.jsp" %>
            <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

            <div id="description">
                
                <div id="breadcrumb">
                    <p><a href="index.jsp">Home</a> >> <a href="mycart.jsp">MyCart</a> </p>
                </div>
                <c:if test="${theShoppingCart.count gt 0}">
                <h3>Your Items</h3>
                <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
                <form action="OrderController">

                <table class="order_table">
                    <tr>
                        <th class="item_col">Item</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Total</th>
                    </tr>
                    <c:forEach var="orderItem" items="${theShoppingCart.itemList}">
                    <tr>
                        <td class="item_col">${orderItem.product.productName}
                            <input type="hidden" name="productList" value="${orderItem.product.productCode}" /></td>
                        <td class="align_right">${orderItem.product.price}</td>
                        <td class="align_right"><input type="text" name="${orderItem.product.productCode}" value="${orderItem.quantity}" size="2" class="align_right"/></td>
                        <td class="align_right">${orderItem.product.price * orderItem.quantity}</td>
                        
                    </tr>
                    </c:forEach>
                    <tr>
                        <td class="item_col"> </td>
                        <td class="align_right"> </td>
                        <td class="align_right">Subtotal</td>
                        <td class="align_right">${theShoppingCart.totalPrice}</td>
                    </tr>
                </table>
                    <button type="submit" name="action" value="updateCart" class="button">Update Cart</button>
                    <button type="submit" name="action" value="checkout" class="button">Checkout</button>
                </form>
                    </c:if>
                <c:if test="${theShoppingCart.count eq 0 || theShoppingCart eq null}">
                    
                    <h1>Your Cart is Empty</h1>
                    <a href="CatalogControllerServlet" class="button">back</a>
                    
                </c:if>
                
            </div>
         </div>


         <%@ include file="footer.jsp" %>
             </div>
    </body>
</html>