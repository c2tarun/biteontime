<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<nav>
    <ul>
        <li><a href="signup.jsp">Sign In</a></li>
        <li><a href="mycart.jsp">Cart
                <c:if test="${theShoppingCart.count gt 0}">
                    <span>(${theShoppingCart.count})</span>
                </c:if>
            </a>
        </li>
        <li ><a href="OrderController?action=viewOrders">My Orders</a></li>
    </ul>
</nav>