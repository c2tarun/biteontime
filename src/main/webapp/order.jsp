<!DOCTYPE html>
<html>
    <head>
        <title>Bite on Time</title>
<!--        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">-->
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <link rel="stylesheet" type="text/css" href="css/order.css">
        <link rel="stylesheet" type="text/css" href="css/buttons.css">
    </head>
    <body>
    <div id="super">
    <%@ include file="header.jsp" %>
        <%@ include file="user-navigation.jsp" %>
        <div id="main_page">

            <%@ include file="site-navigation.jsp" %>  
            <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
            <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
            
            <div id="description">
                <div id="breadcrumb">
                    <p><a href="index.jsp">Home</a> >> <a href="order.jsp">Order</a> </p>
                </div>
                <h3>Invoice</h3>
        
                <fmt:formatDate type="date" value="${currentOrder.date}" />
        
                <h3>Ship/Bill To</h3>
                Mr. ${theUser.firstName}&nbsp;${theUser.lastName} <br>
                ${theUser.addressField1} <br>
                ${theUser.addressField2} <br>
                ${theUser.city} <br>
                ${theUser.state}-${theUser.postCode}<br>
                <br><br>
               
                <form action="OrderController">
                
                <table class="order_table">
                    <tr>
                        <th class="item_col">Item</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Total</th>
                    </tr>
                    <c:forEach var="orderItem" items="${currentOrder.itemList}">
                    <tr>
                        <td class="item_col">${orderItem.product.productName}</td>
                        <td class="align_right">${orderItem.product.price}</td>
                        <td class="align_right"><input type="text" name="quantity" value="${orderItem.quantity}" size="2" class="align_right" disabled/></td>
                        <td class="align_right">${orderItem.product.price * orderItem.quantity}</td>
                    </tr>
                    </c:forEach>
                    <tr>
                        <td class="item_col"> </td>
                        <td class="align_right"> </td>
                        <td class="align_right">Total Price</td>
                        <td class="align_right">${currentOrder.totalPrice}</td>
                    </tr>
                    <tr>
                        <td class="item_col"> </td>
                        <td class="align_right"> </td>
                        <td class="align_right">Taxes</td>
                        <td class="align_right"><fmt:formatNumber value="${currentOrder.totalTax}" currencySymbol="$" type="currency"/></td>
                    </tr>
                    <tr>
                        <td class="item_col"> </td>
                        <td class="align_right"> </td>
                        <td class="align_right">Subtotal</td>
                        <td class="align_right"><fmt:formatNumber value="${currentOrder.totalPrice + currentOrder.totalTax}" currencySymbol="$" type="currency"/></td>
                    </tr>
                </table>
                <p>
                    <a href="mycart.jsp" class="button">Back to Cart</a>
                    <button type="submit" name="action" value="purchase" class="button">Purchase</button>
                </p>
                </form>
            </div> 
        </div>
<%@ include file="footer.jsp" %>
    </div>
    </body>
</html>