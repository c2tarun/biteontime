<!DOCTYPE html>
<html>
    <head>
        <title>Bite on Time</title>
<!--        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">-->
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <link rel="stylesheet" type="text/css" href="css/catalog.css">
        <link rel="stylesheet" type="text/css" href="css/buttons.css">
<!--        <script type="text/javascript" src="js/jquery-2.1.1.min.js"/>
        <script type="text/javascript" src="js/script.js"/>-->
    </head>
    <body>
    <div id="super">
<%@ include file="header.jsp" %>


       <%@ include file="user-navigation.jsp" %>
        <%@ include file="site-navigation.jsp" %>

        <div id="main_page">

            
            
            <div id="description">
                <div id="breadcrumb">
                    <p><a href="index.jsp">Home</a> >> <a href="CatalogControllerServlet">Catalog</a> </p>
                </div>
                <form action="CatalogControllerServlet" method="post">
                    <select name="catalogCategory" class="style_selector">
                        <option value="all">All</option>
                        <c:forEach var="catalog" items="${catalogs}">
                        <option value="${catalog.catalogName}">${catalog.catalogName}</option>
                        </c:forEach>
                    </select>
                    <input type="submit" value="Apply Filter" class="button_thin">
                </form>
                <br>
                <ol>
                    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
                    <c:forEach var="product" items="${products}">
                    <li>                                                 
                    <table>
                        <tr>
                            <th rowspan="2"><a href="CatalogControllerServlet?productCode=${product.productCode}"><img class="product_image" src="${product.imgURL}" alt="${product.description}"></a></th>
                            <th colspan="2" class="pro_description"><a href="CatalogControllerServlet?productCode=${product.productCode}">${product.productName}</a>
                                <input type="hidden" name="productList" value="${product.productCode}"/></th>
                        </tr>
                        <tr>
                            <td>Nutrition: 100 Cal</td>
                            <td class = "price">
                                <table>
                                    <tr>
                                        <td>${product.price}</td>
                                        <td><a href="OrderController?productCode=${product.productCode}&amp;action=addToCart"><img src="images/add-to-cart-icon.jpg" alt="add to card"></a></td>
                                    </tr>
                                    <c:if test="${theShoppingCart.isProductInCart(product.productCode)}">
                                    <tr> 
                                        <td colspan="2">
                                            <p class="product_status">Item in Cart.</p>
                                        </td>
                                    </tr>
                                    </c:if>
                                </table>
                            </td>
                        </tr>
                    </table>
                    </li>
                    </c:forEach>
                
                </ol>
                
            </div>
         </div>


<%@ include file="footer.jsp" %>
    </div>
    </body>
</html>