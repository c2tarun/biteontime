<!DOCTYPE html>
<html>
<head>
    <title>Bite on Time</title>
<!--    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">-->
    <link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>
    <div id="super">
        <%@ include file="header.jsp" %>
        <%@ include file="user-navigation.jsp" %>

        <div id="main_page">

            <%@ include file="site-navigation.jsp" %>
            
            <div id="description">
            <div id="breadcrumb">    
                <p><a href="index.jsp">Home</a> >> <a href="about.jsp">About Us</a></p>
            </div>
                <h3>   About</h3>

                <p>BITE ON TIME have emerged from customer needs. When you go to a place, having food which you love adds a new flavor to life. We used to think that there should be an online store to get your favorite beverage delivered anytime, anywhere. Then we thought maybe we should start this on our own. And here we are. :)
                    <br>A bunch of students with an innovative idea to deliver "Bite On Time"</p>               


                </div>  


                <%@ include file="footer.jsp" %>

            </div>
        </div>
        </body>
        </html>
