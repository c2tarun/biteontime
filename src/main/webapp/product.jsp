<!DOCTYPE html>
<html>
    <head>
        <title>Bite on Time</title>
<!--        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">-->
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <link rel="stylesheet" type="text/css" href="css/product.css">
    </head>
    <body>
    <div id="super">
<%@ include file="header.jsp" %>


        <%@ include file="user-navigation.jsp" %>
        

        <div id="main_page">

            <%@ include file="site-navigation.jsp" %>
            <div id="description">
                <h3>Starbucks Vanilla Iced Coffee</h3>
                    <table>
                        <tr>
                            <td rowspan="2"><img src="images/vanilla_IC_SB.jpg" alt="Vanilla Ice Cream"></td>
                            <td class="align_right">$7</td>
                        </tr>
                        <tr>
                            <td class="align_right"><img src="images/add-to-cart-icon.jpg" alt="Add to cart"></td>
                        </tr>
                    </table>
                    <br><br><br>

                    <table>
                        <tr>
                        <th>Nutrition Table</th>
                        </tr>
                        <tr>
                            <td>Calories</td>
                            <td>100</td>
                        </tr>
                        <tr>
                            <td>Total Fat</td>
                            <td>.5gm</td>
                        </tr> 

                    </table>

                    <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
                
            </div>
         </div>


<%@ include file="footer.jsp" %>
    </div>
    </body>
</html>